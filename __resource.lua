resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/HondaCivicTypeREK9/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/HondaCivicTypeREK9/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/HondaCivicTypeREK9/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/HondaCivicTypeREK9/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/HondaCivicTypeREK9/carvariations.meta'

files {
'data/HondaCivicTypeREK9/handling.meta',
'data/HondaCivicTypeREK9/vehicles.meta',
'data/HondaCivicTypeREK9/carcols.meta',
'data/HondaCivicTypeREK9/carvariations.meta',
'data/HondaCivicTypeREK9/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/LancerEvolutionX/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/LancerEvolutionX/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/LancerEvolutionX/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/LancerEvolutionX/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/LancerEvolutionX/carvariations.meta'

files {
'data/LancerEvolutionX/handling.meta',
'data/LancerEvolutionX/vehicles.meta',
'data/LancerEvolutionX/carcols.meta',
'data/LancerEvolutionX/carvariations.meta',
'data/LancerEvolutionX/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/LexusSc300/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/LexusSc300/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/LexusSc300/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/LexusSc300/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/LexusSc300/carvariations.meta'

files {
'data/LexusSc300/handling.meta',
'data/LexusSc300/vehicles.meta',
'data/LexusSc300/carcols.meta',
'data/LexusSc300/carvariations.meta',
'data/LexusSc300/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/MazdaRX7FC3/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/MazdaRX7FC3/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/MazdaRX7FC3/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/MazdaRX7FC3/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/MazdaRX7FC3/carvariations.meta'

files {
'data/MazdaRX7FC3/handling.meta',
'data/MazdaRX7FC3/vehicles.meta',
'data/MazdaRX7FC3/carcols.meta',
'data/MazdaRX7FC3/carvariations.meta',
'data/MazdaRX7FC3/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/MazdaRX7202Spirit/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/MazdaRX7202Spirit/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/MazdaRX7202Spirit/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/MazdaRX7202Spirit/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/MazdaRX7202Spirit/carvariations.meta'

files {
'data/MazdaRX7202Spirit/handling.meta',
'data/MazdaRX7202Spirit/vehicles.meta',
'data/MazdaRX7202Spirit/carcols.meta',
'data/MazdaRX7202Spirit/carvariations.meta',
'data/MazdaRX7202Spirit/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/NissanGTR/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/NissanGTR/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/NissanGTR/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/NissanGTR/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/NissanGTR/carvariations.meta'

files {
'data/NissanGTR/handling.meta',
'data/NissanGTR/vehicles.meta',
'data/NissanGTR/carcols.meta',
'data/NissanGTR/carvariations.meta',
'data/NissanGTR/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/Rx7/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Rx7/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Rx7/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Rx7/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Rx7/carvariations.meta'

files {
'data/Rx7/handling.meta',
'data/Rx7/vehicles.meta',
'data/Rx7/carcols.meta',
'data/Rx7/carvariations.meta',
'data/Rx7/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/S15/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/S15/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/S15/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/S15/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/S15/carvariations.meta'

files {
'data/S15/handling.meta',
'data/S15/vehicles.meta',
'data/S15/carcols.meta',
'data/S15/carvariations.meta',
'data/S15/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/Skyline/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Skyline/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Skyline/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Skyline/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Skyline/carvariations.meta'

files {
'data/Skyline/handling.meta',
'data/Skyline/vehicles.meta',
'data/Skyline/carcols.meta',
'data/Skyline/carvariations.meta',
'data/Skyline/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/SkylineER34/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/SkylineER34/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/SkylineER34/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/SkylineER34/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/SkylineER34/carvariations.meta'

files {
'data/SkylineER34/handling.meta',
'data/SkylineER34/vehicles.meta',
'data/SkylineER34/carcols.meta',
'data/SkylineER34/carvariations.meta',
'data/SkylineER34/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/SportsCivicSIR/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/SportsCivicSIR/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/SportsCivicSIR/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/SportsCivicSIR/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/SportsCivicSIR/carvariations.meta'

files {
'data/SportsCivicSIR/handling.meta',
'data/SportsCivicSIR/vehicles.meta',
'data/SportsCivicSIR/carcols.meta',
'data/SportsCivicSIR/carvariations.meta',
'data/SportsCivicSIR/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/SubaruGT/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/SubaruGT/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/SubaruGT/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/SubaruGT/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/SubaruGT/carvariations.meta'

files {
'data/SubaruGT/handling.meta',
'data/SubaruGT/vehicles.meta',
'data/SubaruGT/carcols.meta',
'data/SubaruGT/carvariations.meta',
'data/SubaruGT/vehiclelayouts.meta',
}
---------------------------------------------------------------------





client_script 'vehicle_names.lua'